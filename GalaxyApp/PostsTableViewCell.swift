//
//  PostsTableViewCell.swift
//  GalaxyApp
//
//  Created by Nerses Zakoyan on 25.04.16.
//  Copyright © 2016 Galaxy. All rights reserved.
//

import UIKit

class PostsTableViewCell: UITableViewCell {

    // MARK: - Properties

    
    @IBOutlet weak var pictureHeight: NSLayoutConstraint!
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var updatedTimeLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var postImg: UIImageView!
    @IBOutlet weak var linkLbl: UILabel!
}
