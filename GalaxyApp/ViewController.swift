//
//  ViewController.swift
//  GalaxyApp
//
//  Created by Nerses Zakoyan on 21.04.16.
//  Copyright © 2016 Galaxy. All rights reserved.
//

import UIKit
import TwitterKit
import InstagramKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var postsTable: UITableView!
    let dateFormatter = NSDateFormatter()
    var posts:[Post]=[]
    

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        let bounds = UIApplication.sharedApplication().statusBarFrame
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
        visualEffectView.frame = bounds
        visualEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.navigationController?.navigationBar.addSubview(visualEffectView)
        
        
        postsTable.delegate = self
        postsTable.dataSource = self
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        InstagramEngine.sharedEngine().accessToken = defaults.objectForKey("token") as? String
        
        loginChanged(PostType.FACEBOOK)
        loginChanged(PostType.TWITTER)
        loginChanged(PostType.INSTAGRAM)
        
        postsTable.rowHeight = UITableViewAutomaticDimension
        postsTable.estimatedRowHeight = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = postsTable.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PostsTableViewCell
        
        let post = posts[indexPath.row]
        
        switch (post.type) {
        case .FACEBOOK :
            cell.iconImg.image = UIImage(named: "fbsign")
        case .TWITTER :
            cell.iconImg.image = UIImage(named: "twrsign")
        case .INSTAGRAM :
            cell.iconImg.image = UIImage(named: "instasign")
        }
        
        cell.avatarImg.image = UIImage(data: NSData(contentsOfURL: NSURL(string: post.avatar!)!)!)
        
        dateFormatter.dateFormat = "d MMMM 'at' hh:mm"
        cell.updatedTimeLbl.text = post.name + " · " + dateFormatter.stringFromDate(post.updatedTime)
        cell.messageLbl.text = post.message
        if (post.picture != nil) {
            let image = UIImage(data: NSData(contentsOfURL: NSURL(string: post.picture!)!)!)
            let newHeight = (self.view.frame.width - 162)/image!.size.width * image!.size.height
            cell.pictureHeight.constant = newHeight
            cell.postImg.image = image
        } else if (post.url != nil) {
            cell.linkLbl.text = post.url
            cell.pictureHeight.constant = 0
        } else {
            cell.pictureHeight.constant = 0
        }
        cell.messageLbl.sizeToFit()
        
        let height = ceil(cell.systemLayoutSizeFittingSize(CGSizeMake(self.postsTable.bounds.size.width, 1), withHorizontalFittingPriority: 1000, verticalFittingPriority: 1).height) + cell.pictureHeight.constant
        cell.contentView.frame.size = CGSizeMake(cell.contentView.frame.height, height)
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        label.hidden = posts.count > 0
        return posts.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if (posts[indexPath.row].url != nil) {
            UIApplication.sharedApplication().openURL(NSURL(string:posts[indexPath.row].url!)!)
        }
    }
    
    
    func loginChanged(type:PostType){
        switch (type) {
        case .FACEBOOK : if FBSDKAccessToken.currentAccessToken() != nil {
            loadFacebook()
        } else {
            removePosts(type)
            }
        case .TWITTER : if Twitter.sharedInstance().sessionStore.session() != nil {
            loadTwitter()
        } else {
            removePosts(type)
            }
        case .INSTAGRAM : if InstagramEngine.sharedEngine().isSessionValid() {
            
            loadInstagram()
        } else {
            removePosts(type)
            }
        }
    }
    
    func removePosts(type:PostType){
        posts = posts.filter({$0.type != type})
        postsTable.reloadData()
    }
    
    func loadFacebook(){
        let fbRequest = FBSDKGraphRequest(graphPath: "me/friends", parameters: ["fields":"id,name,picture,posts.limit(1){message,link,picture,updated_time}", "limit":"1000"]);
        
        fbRequest.startWithCompletionHandler { (connection : FBSDKGraphRequestConnection!, result : AnyObject!, error : NSError!) -> Void in
            
            if error == nil {
                let resultdict = result as! NSDictionary
                
                let data : NSArray = resultdict.objectForKey("data") as! NSArray
                
                for i in 0..<data.count {
                    
                    
                    let dict : NSDictionary = data[i] as! NSDictionary
                    
                    let avatar = ((dict.objectForKey("picture"))?.objectForKey("data"))?.objectForKey("url") as! String
                    let name = dict.objectForKey("name") as! String
                    let value = (dict.objectForKey("posts") as! NSDictionary).objectForKey("data") as! NSArray
                    let valueDict = value[0] as! NSDictionary
                    let message = valueDict.objectForKey("message") as? String
                    let updatedTime = valueDict.objectForKey("updated_time") as! String
                    let link = valueDict.objectForKey("link") as? String
                    let picture = valueDict.objectForKey("picture") as? String
                    self.dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ssZ"
                    let dateObj = self.dateFormatter.dateFromString(updatedTime)
                    
                    let post = Post()
                    post.name = name
                    post.type = PostType.FACEBOOK
                    post.avatar = avatar
                    post.message = message
                    post.url = link
                    post.picture = picture
                    post.updatedTime = dateObj
                    self.posts.insert(post, atIndex: self.posts.count)
                    
                }
                self.postsTable.reloadData()
                self.posts.sortInPlace({ $0.updatedTime.compare($1.updatedTime) == NSComparisonResult.OrderedDescending })
            } else {
                MessageToUser.showDefaultErrorMessage()
            }
        }
    }
    
    func loadTwitter(){
        
        let client = TWTRAPIClient.init(userID: Twitter.sharedInstance().sessionStore.session()?.userID)
        let statusesShowEndpoint = "https://api.twitter.com/1.1/friends/list.json"
        let params = ["cursor": "-1", "count":"5000"]
        var clientError : NSError?
        
        let request = client.URLRequestWithMethod("GET", URL: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError != nil {
                MessageToUser.showDefaultErrorMessage()
                print("Error: \(connectionError)")
            }
            
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                
                guard let users = json["users"] as? [[String : AnyObject]] else {
                    return
                }
                
                for user in users {
                    let userDict: NSDictionary = user as NSDictionary
                    if let status = userDict.objectForKey("status") as? NSDictionary {
                        
                        guard let name = userDict.objectForKey("name") as? String else {
                            return
                        }
                        
                        let avatar = userDict.objectForKey("profile_image_url") as? String
                        
                        
                        let statusDict: NSDictionary = status as NSDictionary
                        
                        guard let updatedTime = statusDict.objectForKey("created_at") as? String else {
                            return
                        }
                        
                        let message = statusDict.objectForKey("text") as? String
                        
                        let entities = statusDict.objectForKey("entities") as! [String: AnyObject]
                        let entitiesDict: NSDictionary = entities as NSDictionary
                        
                        let media = entitiesDict.objectForKey("media") as? [[String: AnyObject]]
                        var url: String? = nil
                        var picture: String? = nil
                        
                        if media == nil {
                            if let urls = entitiesDict.objectForKey("urls") as? [[String: AnyObject]] {
                                if urls.count>0 {
                                    let urlDict: NSDictionary = urls[0] as NSDictionary
                                    url = (urlDict.objectForKey("expanded_url") as? String)!
                                } else {
                                    url = userDict.objectForKey("url") as! String
                                }
                            }
                        }
                        else {
                            let mediaDict: NSDictionary = media![0] as NSDictionary
                            picture = (mediaDict.objectForKey("media_url") as? String)!
                            url = (mediaDict.objectForKey("url") as? String)!
                        }
                        
                        self.dateFormatter.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"
                        let dateObj = self.dateFormatter.dateFromString(updatedTime)
                        
                        let post = Post()
                        post.name = name
                        post.type = PostType.TWITTER
                        post.avatar = avatar
                        post.message = message
                        post.url = url
                        post.picture = picture
                        post.updatedTime = dateObj
                        self.posts.insert(post, atIndex: self.posts.count)
                    }
                }
                self.posts.sortInPlace({ $0.updatedTime.compare($1.updatedTime) == NSComparisonResult.OrderedDescending })
                self.postsTable.reloadData()
                
            } catch let jsonError as NSError {
                print("json error: \(jsonError.localizedDescription)")
            }
        }
    }
    
    func loadInstagram(){
        Alamofire.request(.GET, "https://api.instagram.com/v1/users/self/follows", parameters: ["access_token": InstagramEngine.sharedEngine().accessToken!])
            .responseJSON { response in
                
                if let users = (response.result.value as? NSDictionary)?.objectForKey("data") as? [NSDictionary] {
                    for user in users {
                        let id:String = user.objectForKey("id") as! String
                        
                        self.loadUser(id)
                    }
                }
        }
    }
    
    func loadUser(id:String) {
        Alamofire.request(.GET, "https://api.instagram.com/v1/users/\(id)/media/recent/", parameters: ["access_token": InstagramEngine.sharedEngine().accessToken!])
            .responseJSON { response in
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                }
                if let data = (response.result.value as? NSDictionary)?.objectForKey("data") as? [NSDictionary] {
                    if data.count > 0 {
                    let media = data[0]
                    let userObject = media.objectForKey("user") as! NSDictionary
                    var name:String = userObject.objectForKey("full_name") as! String
                    if name.isEmpty {
                        name = userObject.objectForKey("username") as! String
                    }
                    let avatar = userObject.objectForKey("profile_picture") as! String
                    let url = media.objectForKey("link") as! String
                    
                    let post = Post()
                    post.name = name
                    post.type = PostType.INSTAGRAM
                    post.avatar = avatar
                    if let message = media.objectForKey("caption") as? String {
                        post.message = message
                    }
                    post.url = url
                    
                    post.picture = ((media.objectForKey("images") as! NSDictionary).objectForKey("low_resolution") as! NSDictionary).objectForKey("url") as! String
                    post.updatedTime = NSDate(timeIntervalSince1970: Double(media.objectForKey("created_time") as! String)!)
                    self.posts.insert(post, atIndex: self.posts.count)
                    self.posts.sortInPlace({ $0.updatedTime.compare($1.updatedTime) == NSComparisonResult.OrderedDescending })
                    self.postsTable.reloadData()
                }
                }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        (segue.destinationViewController as! SettingsViewController).vc = self
    }
    
}

