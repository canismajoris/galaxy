//
//  InstagramViewController.swift
//  GalaxyApp
//
//  Created by Nerses Zakoyan on 26.04.16.
//  Copyright © 2016 Galaxy. All rights reserved.
//

import Foundation
import UIKit
import InstagramKit

protocol InstagramDelegate : NSObjectProtocol {
    func instagramSuccess(user:InstagramUser)
    func instagramFailure()
}

class InstagramViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    var delegate:InstagramDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        
        let scopes = InstagramKitLoginScope.PublicContent.rawValue | InstagramKitLoginScope.FollowerList.rawValue
        let requestURL = InstagramEngine.sharedEngine().authorizationURLForScope(InstagramKitLoginScope(rawValue: scopes))
        
        let request = NSURLRequest(URL: requestURL)
        webView.loadRequest(request)
    }
    
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let urlString:String! = request.URL!.absoluteString
        do {
            try InstagramEngine.sharedEngine().receivedValidAccessTokenFromURL(request.URL!)
            //            let token = urlString.substringFromIndex(range.endIndex)
            let engine = InstagramEngine.sharedEngine()
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(engine.accessToken, forKey: "token")
            defaults.synchronize()
            //            engine.accessToken = token
            engine.getSelfUserDetailsWithSuccess({
                (user:InstagramUser?) in
                
                
                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    self.delegate!.instagramSuccess(user!)
                })
                },
                                                 failure: {
                                                    (error:NSError?, statusCode:Int) in
                                                    print(error, terminator: "")
            })
            
            return false
        } catch {
            return true
            
        }
        
        //        var error:NSErrorPointer = NSErrorPointer()
        //        if (InstagramEngine.sharedEngine().extractValidAccessTokenFromURL(request.URL, error: error)) {
        //            if (error != nil) {
        //
        //            } else {
        //
        //            }
        //        }
        return true
        
        //
        //
        //        let urlString:String! = request.URL!.absoluteString
        //
        //        if let range:Range<String.Index> = urlString.rangeOfString("access_token=") {
        //            delegate!.instagramSuccess(urlString.substringFromIndex(range.endIndex))
        //            dismissViewControllerAnimated(true, completion: nil)
        //            return false
        //        }
        //
        //        return true
    }
    
    
    @IBAction func close(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}