//
//  SettingsViewController.swift
//  GalaxyApp
//
//  Created by Nerses Zakoyan on 25.04.16.
//  Copyright © 2016 Galaxy. All rights reserved.
//

import UIKit
import TwitterKit
import FBSDKLoginKit
import InstagramKit

class SettingsViewController: UIViewController, FBSDKLoginButtonDelegate, InstagramDelegate {
    
    var vc:ViewController!
    
    @IBOutlet weak var fbButton: FBSDKLoginButton!
    
    @IBOutlet weak var twrButton: TWTRLogInButton!
    
    @IBOutlet weak var instaButton: UIButton!
    
    @IBOutlet weak var logoutFacebook: UIButton!
    
    @IBOutlet weak var logoutTwitter: UIButton!
    
    @IBOutlet weak var logoutInstagram: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fbButton.readPermissions = ["public_profile", "user_friends", "user_posts"]
        fbButton.delegate = self
        
        
        if FBSDKAccessToken.currentAccessToken() != nil {
            fbButton.hidden = true
            logoutFacebook.hidden = false
        } else {
            fbButton.hidden = false
            logoutFacebook.hidden = true
        }
        
        if Twitter.sharedInstance().sessionStore.session() != nil {
            twrButton.hidden = true
            logoutTwitter.hidden = false
        } else {
            twrButton.hidden = false
            logoutTwitter.hidden = true
        }
        
        if InstagramEngine.sharedEngine().isSessionValid() {
            instaButton.hidden = true
            logoutInstagram.hidden = false
        } else {
            instaButton.hidden = false
            logoutInstagram.hidden = true
        }
        
        
        
        twrButton.logInCompletion = { session, error in
            if (session != nil) {
                self.twrButton.hidden = true
                self.logoutTwitter.hidden = false
                self.dismissViewControllerAnimated(true, completion: {
                    self.vc.loginChanged(PostType.TWITTER)
                })
                print("signed in as \(session!.userName)");
            } else {
                print("error: \(error!.localizedDescription)");
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginButton(loginButton: FBSDKLoginButton!,
                     didCompleteWithResult result: FBSDKLoginManagerLoginResult!,
                                           error: NSError!) {
        
        if ((error) != nil) {
            MessageToUser.showDefaultErrorMessage();
        } else if result.isCancelled {
            // Handle cancellations
        } else {
            fbButton.hidden = true
            logoutFacebook.hidden = false
            dismissViewControllerAnimated(true, completion: {
                self.vc.loginChanged(PostType.FACEBOOK)
            })
        }
    }
    internal func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        fbButton.hidden = false
        logoutFacebook.hidden = true
        dismissViewControllerAnimated(true, completion: {
            self.vc.loginChanged(PostType.FACEBOOK)
        })
    }
    
    @IBAction func logoutFacebook(sender: AnyObject) {
        FBSDKLoginManager().logOut()
        fbButton.hidden = false
        logoutFacebook.hidden = true
        dismissViewControllerAnimated(true, completion: {
            self.vc.loginChanged(PostType.FACEBOOK)
        })
    }
    
    @IBAction func logoutTwitter(sender: AnyObject) {
        for session in Twitter.sharedInstance().sessionStore.existingUserSessions() {
            Twitter.sharedInstance().sessionStore.logOutUserID((session as! TWTRSession).userID)
        }
        twrButton.hidden = false
        logoutTwitter.hidden = true
        dismissViewControllerAnimated(true, completion: {
            self.vc.loginChanged(PostType.TWITTER)
        })
    }
    
    
    @IBAction func logOutInstagram(sender: AnyObject) {
        InstagramEngine.sharedEngine().logout()
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey("token")
        defaults.synchronize()
        instaButton.hidden = false
        logoutInstagram.hidden = true
        dismissViewControllerAnimated(true, completion: {
            self.vc.loginChanged(PostType.INSTAGRAM)
        })
    }
    
    func instagramSuccess(user:InstagramUser) {
        
        instaButton.hidden = true
        logoutInstagram.hidden = false
        dismissViewControllerAnimated(true, completion: {
            self.vc.loginChanged(PostType.INSTAGRAM)
        })
    }
    func instagramFailure() {
        MessageToUser.showDefaultErrorMessage()
    }
    
    
    
    @IBAction func done(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc = segue.destinationViewController as! InstagramViewController
        vc.delegate = self
    }
    
    
}
