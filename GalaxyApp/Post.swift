//
//  Post.swift
//  GalaxyApp
//
//  Created by Nerses Zakoyan on 25.04.16.
//  Copyright © 2016 Galaxy. All rights reserved.
//

import Foundation
class Post {
    
    var name: String!
    var type: PostType = PostType.FACEBOOK
    var avatar: String?
    var message: String?
    var url: String?
    var picture: String?
    var updatedTime: NSDate!
    
}