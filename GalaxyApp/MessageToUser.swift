//
//  MessageToUser.swift
//  GalaxyApp
//
//  Created by Nerses Zakoyan on 25.04.16.
//  Copyright © 2016 Galaxy. All rights reserved.
//

import Foundation
import UIKit
class MessageToUser {
    
    static func showMessage(title:String, textId:String!) {
        let alert = UIAlertView()
        alert.title = title
        alert.message = textId
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
    static func showErrorMessage(textId:String!) {
        let alert = UIAlertView()
        alert.title = "Ooops"
        alert.message = textId
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
    static func showDefaultErrorMessage() {
        let alert = UIAlertView()
        alert.title = "Ooops"
        alert.message = "Something went wrong. Please try again later."
        alert.addButtonWithTitle("OK")
        alert.show()
    }
    
}