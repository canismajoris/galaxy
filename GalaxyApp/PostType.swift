//
//  PostType.swift
//  GalaxyApp
//
//  Created by Nerses Zakoyan on 25.04.16.
//  Copyright © 2016 Galaxy. All rights reserved.
//

import Foundation
enum PostType:Int {
    
    case FACEBOOK, TWITTER, INSTAGRAM
}